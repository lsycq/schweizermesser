import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1
import Qt.UsbSerial 1.0

Rectangle {
    id: root;
//    property alias sendTextField: sendTextField;
//    property alias recvTextField: recvTextField;
    property var recvSend: "";
    property int canType: 1;
    Column {
        id: sendColum;
        x: 2;
        y: 4;
        spacing: 4;
        GroupBox {
            id: viewGroupBox;
            width: 650;
            height: 80;
            Column {
                y: 4;
                spacing: 6;
                Row {
                    x: 4;
                    spacing: 4;
                    Text {
                        id: frameFormatText;
                        y: 6;
                        text: qsTr("帧格式: ");
                    }
                    ComboBox {
                        id: frameFormatCombo;
                        width: 80;
                        model: ["标准帧", "扩展帧"];
                    }
                    Text {
                        id: frameTypeText;
                        y: 6;
                        text: qsTr("  帧类型: ");
                    }
                    ComboBox {
                        id: frameTypeCombo;
                        width: 80;
                        model: ["数据帧", "远程帧"];
                    }
                    Text {
                        id: frameIDText;
                        y: 6;
                        text: qsTr("  帧ID: ");
                    }
                    TextField {
                        id: frameIDTextEdit;
                        width: 80;
                        height: 25;
                        text: "123456";
                    }
                    Text {
                        id: canText;
                        y: 6;
                        text: qsTr(" CAN通道: ");
                    }
                    ComboBox {
                        id: canCombo;
                        width: 60;
                        model: ["CAN1",  "CAN2"];
                        onCurrentIndexChanged: {
                            configSend();
                        }
                    }
                    Button {
                        id: parameterConfigButton;
                        width: 60;
                        text: "参数配置";
                        onClicked: {
                            var data;
                            var filter = functionItem.idData("0");
                            var mask = functionItem.idData("0");
                            data = Number(canCombo.currentIndex + 1).toString(16) + " " + Number(baudCombox.currentIndex + 1).toString(16) +  " " + filter + " " + mask;
                            functionItem.sendData(canType, 3, data, UsbSerial.HexStringType);
                        }
                    }
                }
                Row {
                    spacing: 4;
                    Text {
                        id: sendDataText;
                        y: 6;
                        text: qsTr("数据: ");
                    }
                    TextField {
                        id: sendDataTextEdit;
                        width: 160;
                        height: 25;
                        text: "01 02 03 04";
                    }
                    Button {
                        id: sendButton;
                        width: 64;
                        text: "发送";
                        onClicked: {
                            var data;
                            data = sendCanData(canCombo.currentIndex, frameIDTextEdit.text,  frameFormatCombo.currentIndex,
                                                   frameTypeCombo.currentIndex, sendDataTextEdit.text);
                            console.log("click send can button: " + data);
                            functionItem.sendData(canType, 4, data, UsbSerial.HexStringType);
                            recvSend = "发送";
                            sendTextField.text = (Number(sendTextField.text) + 1).toString();
                            listView.add(data);
                        }
                        function sendCanData(can, frameID, frameFormat,frameType, data) {
                            var dataString;
                            var typeDataLength;
                            frameID = functionItem.idData(frameID);
                            data = functionItem.formatHexString(data);
                            if (data == "") {
                                return;
                            }
                            data = functionItem.cutHexStr(data);
                            console.log("sendCanData " + data);
                            data = functionItem.dataStr16Str(data);
                            var dataSplit = functionItem.splitSpace(data);
                            var dataLength = dataSplit.length;
                            console.log("sendcandatalength = " + dataLength)
                            if (dataLength > 7) {
                                dataLength = 8;
                                data = "";
                                for (var i = 0; i < dataLength; i++) {
                                    data += " " + dataSplit[i];
                                }
                            }
                            if (frameFormat == 0) {
                                typeDataLength = dataLength;
                            } else {
                                typeDataLength = dataLength + 128;
                            }
                            if (frameType == 1) {
                                typeDataLength = typeDataLength + 64 - dataLength;
                                dataString = (can + 1).toString(16) + " " + frameID + " "  + typeDataLength.toString(16);
                                return dataString;
                            } else {
//                                data = functionItem.dataStringToData(data);
                                dataString = (can + 1).toString(16) + " " + frameID + " " + typeDataLength.toString(16) + data;
                                console.log("sendCanData = " + data)
                                return dataString;
                            }
                        }
                    }
                    Text {
                        id: baudText;
                        y: 4;
                        text: qsTr(" 波特率:")
                    }
                    ComboBox {
                        id:baudCombox;
                        width: 70;
                        model: ["20K", "50K", "100K", "125K", "200K", "250K", "400K", "500K", "1M"];
                    }
                    Text {
                        id: filterText;
                        y: 4;
                        text: qsTr(" 滤波器:")
                    }
                    TextField {
                        id: filterField;
                        width: 60;
                        text: "0";
                        readOnly: true;
                    }
                    Text {
                        id: maskText;
                        y: 4;
                        text: qsTr(" MASK:")
                    }
                    TextField {
                        id: maskField;
                        width: 60;
                        text: "0";
                        readOnly: true;
                    }
                }
            }
        }
        GroupBox {
            id: dataViewGroupBox;
            width: viewGroupBox.width;
            height: root.height - viewGroupBox.height - 30;

            Component {
                id:pathDelegate
                Item {
                    id:wrapper
                    width: parent.width;
                    height: 20;
                    MouseArea {
                        anchors.fill: parent;
                        onClicked: {
                            wrapper.ListView.view.currentIndex = index;
                        }
                    RowLayout {
                        anchors.left: parent.left;
                        anchors.verticalCenter: parent.verticalCenter;
                        spacing: 8;
                        Text {
                            text: index;
                            color: wrapper.ListView.isCurrentItem ? "red" : "black";
                            font.pixelSize: /*wrapper.ListView.isCurrentItem ? 15 : */12;
                            Layout.preferredWidth: 32;
                        }
                        Text {
                            text: channel;
                            color: wrapper.ListView.isCurrentItem ? "red" : "black";
                            font.pixelSize: /*wrapper.ListView.isCurrentItem ? 15 : */12;
                            Layout.preferredWidth: 50;
                        }
                        Text {
                            text: rw;
                            color: wrapper.ListView.isCurrentItem ? "red" : "black";
                            font.pixelSize: /*wrapper.ListView.isCurrentItem ? 15 : */12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 70;
                        }
                        Text {
                            text: format;
                            color: wrapper.ListView.isCurrentItem ? "red" : "black";
                            font.pixelSize: /*wrapper.ListView.isCurrentItem ? 15 : */12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 51;
                        }
                        Text {
                            text: type;
                            color: wrapper.ListView.isCurrentItem ? "red" : "black";
                            font.pixelSize: /*wrapper.ListView.isCurrentItem ? 15 : */12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 51;
                        }
                        Text {
                            text: id;
                            color: wrapper.ListView.isCurrentItem ? "red" : "black";
                            font.pixelSize: /*wrapper.ListView.isCurrentItem ? 15 : */12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 65;
                        }
                        Text {
                            text: frameData;
                            color: wrapper.ListView.isCurrentItem ? "red" : "black";
                            font.pixelSize: /*wrapper.ListView.isCurrentItem ? 15 : */12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 140;
                        }
                        Text {
                            text: time;
                            color: wrapper.ListView.isCurrentItem ? "red" : "black";
                            font.pixelSize: /*wrapper.ListView.isCurrentItem ? 15 : */12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 100;
                        }
                    }
                    }
                }
            }
            Component {
                id: headerView;
                Rectangle {
                    width: parent.width;
                    height: 20;
//                    color: "#EEEEEE";
                    z: 10;

                    RowLayout {
                        anchors.left: parent.left;
                        anchors.verticalCenter: parent.verticalCenter;
                        Text {
                            text: "序号";
                            color: "black";
                            font.pixelSize: 12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 30;
                        }
                        Text {
                            text: "CAN通道";
                            font.pixelSize: 12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 50;
                        }
                        Text {
                            text: "接收/发送";
                            font.pixelSize: 12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 80;
                        }
                        Text {
                            text: "帧格式";
                            font.pixelSize: 12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 55;
                        }
                        Text {
                            text: "帧类型";
                            font.pixelSize: 12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 55;
                        }
                        Text {
                            text: "帧ID";
                            font.pixelSize: 12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 75;
                        }
                        Text {
                            text: "数据";
                            font.pixelSize: 12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 140;
                        }
                        Text {
                            text: "时间";
                            font.pixelSize: 12;
                            Layout.fillWidth: true;
                            Layout.preferredWidth: 100;
                        }
                    }
                }
            }
            Component {
                id: footerView;
                Item {
                    id: footerItem;
                    width: parent.width;
                    height: 24;

                }
            }
            Component {
                id: dataModel;
                ListModel {

                }
            }
            ListView {
                id: listView;
//                anchors.fill: parent;
                anchors.top: parent.top;
                width: parent.width;
                height: parent.height - 16;
                delegate: pathDelegate;
                header: headerView;
                //footer: footerView;
                model: dataModel.createObject(listView);
                focus: true;
                clip: true;

                headerPositioning: ListView.OverlayHeader;

                highlight: Rectangle {
                    color: "lightblue"
                }
                highlightFollowsCurrentItem: true;
                onCurrentIndexChanged: {
                }
                function getTime() {
                    var oDate = new Date(); //实例一个时间对象；
                    var timeString = oDate.getFullYear().toString() + "/" + (oDate.getMonth() + 1).toString() + "/" + oDate.getDate().toString() + "  "
                            + oDate.getHours().toString() + ":" + oDate.getMinutes().toString() + ":" + oDate.getSeconds().toString();
                    return timeString;
                }
                function add(data) {
                    var arr = [];
                    arr = functionItem.dataStringToArr(data);
                    console.log("add success 1 = " + data);
                    var dataString = "";
                    //帧ID高位用0补全
                    for (var j = 1; j < 5; j++) {
                        if (arr[j].length == 1) {
                            arr[j] = "0" + arr[j];
                        }
                    }
                    var frameId = arr[4] + arr[3] + arr[2] + arr[1];
                    var i;
                    for (i = 6; i < arr.length; i++) {
                        dataString += arr[i].toString() + " ";
                    }
                    var listElement;
                    listElement = {
                        "index": listView.count + 1,
                        "channel": canCombo.textAt(arr[0] - 1),
                        "rw": recvSend,
                        "format": frameFormatCombo.textAt((Number(arr[5]) >> 7) & 1),
                        "type": frameTypeCombo.textAt((Number(arr[5]) >> 6) & 1),
                        "id":frameId,
                        "frameData": dataString,
                        "time": getTime()
                    };
                    listView.model.append(listElement);
                    listView.currentIndex++;
                }
            }
            Button {
                id: clearButton;
                anchors.right: parent.right;
                anchors.rightMargin: 2;
                anchors.bottom: parent.bottom;
                //anchors.bottomMargin: 2;
                height: 20;
                text: "Clear";
                onClicked: {
                    listView.model.clear();
                    sendTextField.text = 0;
                    recvTextField.text = 0;
                    usbSerial.sendCountCAN = 0;
                    usbSerial.recvCountCAN = 0;
                }
            }
            TextEdit {
                id: recvTextField;
                anchors.right: clearButton.left;
                anchors.rightMargin: 6;
                anchors.bottom: clearButton.bottom;
                width: 40;
                text: "0";
                readOnly: true;
            }
            Text {
                id: recvCheckBox;
                anchors.right: recvTextField.left;
                anchors.top: recvTextField.top;
                //anchors.topMargin: 2;
                anchors.rightMargin: 2;
                text: "Recv:";
            }
            TextEdit {
                id: sendTextField;
                anchors.right: recvCheckBox.left;
                anchors.rightMargin: 6;
                anchors.top: recvTextField.top;
                width: 40;
                text: "0";
                readOnly: true;
            }
            Text{
                id: sendCheckBox;
                anchors.right: sendTextField.left;
                anchors.top: recvCheckBox.top;
                anchors.rightMargin: 2;
                text: "Send:";
            }

        }
    }
    function canRecvData(type, cmd, data) {

    }
    function configSend() {
        var can = Number(canCombo.currentIndex + 1).toString();
        functionItem.sendData(canType, 2, can, UsbSerial.HexStringType);
    }
    function updataCanConfig(type, cmd, data) {
        if (type == canType && cmd == 2) {
            var s = data.split(" ");
            console.log("updataCanConfig = " + s[0] + " " + s[1])
            canCombo.currentIndex = parseInt(s[0], 16) - 1;
            baudCombox.currentIndex = parseInt(s[1], 16) - 1;
        } else if (type == canType && cmd == 5) {
            recvSend = "接收";
            recvTextField.text = Number(recvTextField.text) + 1;
            s = data.split(" ");
            console.log("updataCanConfig s = " + s + " " + s.length);
            var dataString= "";
            for (var i = 0; i < s.length; i++) {
                if (s[i].length == 1) {
                    s[i] = "0" + s[i];
                }
                dataString += s[i] + " ";
            }
            console.log("datastring = " + dataString);
            listView.add(dataString);
        } else {
            return -1;
        }
    }
}
