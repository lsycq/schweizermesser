﻿#include "usbserial.h"

UsbSerial::UsbSerial(QObject *parent) : QObject(parent)
{
    m_isOpened = false;
    memset(m_SendCount, 0, sizeof(m_SendCount));
    memset(m_RecvCount, 0, sizeof(m_RecvCount));
}

UsbSerial::~UsbSerial()
{

}

QVariantList UsbSerial::serial_port_scan()
{
    QVariantList name;
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        qDebug() << "Name        : " << info.portName();
        qDebug() << "Description : " << info.description();
        qDebug() << "Manufacturer: " << info.manufacturer();
        qDebug() << "SerialNumber: " << info.serialNumber();
        m_info.push_back(info);
        name.push_back(QVariant(info.portName()));
    }
    return name;
}

QVariantList UsbSerial::scan()
{
    return serial_port_scan();
}

bool UsbSerial::open(QVariant name)
{
    // check name available
    foreach (const QSerialPortInfo &info, m_info) {
        if (name == info.portName()) {
            if (m_isOpened) {
                m_serial.close();
            }
            // serial set port
            m_serial.setPort(info);
            // open
            if (m_serial.open(QIODevice::ReadWrite) == false) {
                qDebug() << "usb serial open failed";
                return false;
            }
            // clear
            m_serial.clearError();
            m_serial.clear();
            m_buffer.clear();   // 清除buffer
            connect(&m_serial, SIGNAL(readyRead()), this, SLOT(readyRead()));
            m_isOpened = true;
            return true;
        }
    }
    return false;
}

bool UsbSerial::close()
{
    if (m_isOpened) {
        m_serial.close();
        m_isOpened = false;
    }
    return true;
}

// 检查是否有一帧数据，如果有，则放到arr里，并从buffer中删除
bool UsbSerial::is_frame_over(QByteArray &arr)
{
    QByteArray &buf = m_buffer;
    // 当数据包大小小于6时返回false
    if (buf.size() < 6) {
        return false;
    }
    if ((quint8)buf[0] != (quint8)0xFA) {
        qDebug() << "Bug: is_frame_over frame head " << (quint8)buf[0];
    }
    while ((quint8)buf[0] != (quint8)0xFA) {
        buf.remove(0, 1);
        if (buf.size() == 0) {
            return false;
        }
    }
    // 没有判断帧尾, bug??
    quint16 len = ((int)buf[1] << 8) | buf[2];
    if (buf.size() < len) {
        return false;
    }
    qDebug() << "is_frame_over: frame len = " << len;
    arr.append(buf.constData(), len);
    buf.remove(0, len);
    return true;
}

void UsbSerial::readyRead()
{
    QByteArray arr = m_serial.readAll();
    if (arr.size() == 0) {
        return;
    }
//    qDebug() << "UsbSerial read: "<< arr;
    m_buffer.append(arr);   // 将新的数据放到buffer后面
    arr.clear();
    // 检查是否满足一帧数据
    while (is_frame_over(arr)) {
        QVariant buf;
        char cmd, type;
        type = arr[3];
        cmd = arr[4];
        if (cmd == 5) {
            cal_recv_count(type, arr);
        }
        arr.remove(0, 5);
        arr.remove(arr.size() - 1, 1);
        qDebug() << "recv serial frame ok, type " << m_out_type;
        switch (m_out_type) {
        case ListType:
            array2valist(buf, arr);
            break;
        case StringType:
            array2string(buf, arr);
            break;
        case HexStringType:
            array2hexstring(buf, arr);
            break;
        default:
            break;
        }
        emit readData(type, cmd, buf);
        arr.clear();
    }
    return;
}

int UsbSerial::valist2array(QByteArray &arr, QVariant &data)
{
    // type return QJSValue
//    if (data.type() != QMetaType::QVariantList) {
//        qDebug() << "valist2array error type " << data.typeName();
//        return -1;
//    }
    QVariantList list = data.toList();
    foreach (const QVariant &v, list) {
        arr.append((char)v.toInt());
    }
    return 0;
}

int UsbSerial::string2array(QByteArray &arr, QVariant &data)
{
    if (data.type() != QMetaType::QString) {
        qDebug() << "string2array error type " << data.typeName();
        //return -1;
    }
    QString s = data.toString();
    arr.append(s);
    return 0;
}

int UsbSerial::hexstring2array(QByteArray &arr, QVariant &data)
{
    if (data.type() != QMetaType::QString) {
        qDebug() << "hexstring2array error type " << data.typeName();
        return -1;
    }
    QString s = data.toString();
    QStringList l = s.split(" ");
    qDebug() << "hexstring2array string list " << l;
    foreach (const QString &str, l) {
        if (str.length() == 0) {
            continue;
        }
        arr.append((char)str.toInt(Q_NULLPTR, 16));
//        qDebug() << "hexstring2array string list str " << str;
    }
    qDebug() << "hexstring2array byte " << arr;
    return 0;
}

int UsbSerial::array2valist(QVariant &data, QByteArray &arr)
{
    QVariantList list;
    foreach (const char &v, arr) {
        list.append(v);
    }
    data = QVariant(list);
    return 0;
}

int UsbSerial::array2string(QVariant &data, QByteArray &arr)
{
    data = QVariant(QString(arr));
    return 0;
}

int UsbSerial::array2hexstring(QVariant &data, QByteArray &arr)
{
    QString s;
    foreach (const char &c, arr) {
        s += QString::number((unsigned char)c, 16);
        s += " ";
    }
    // 去掉最后一个空格
    s.resize(s.size() - 1);
    data = QVariant(s);
    return 0;
}

int UsbSerial::write(char type, char cmd, QVariant buf, DataType t)
{
    QByteArray arr;
    qint16 len;
    int ret;
    qDebug() << "UsbSerial write1: " << t;
    switch (t) {
    case ListType:
        ret = valist2array(arr, buf);
        break;
    case StringType:
        ret = string2array(arr, buf);
        break;
    case HexStringType:
        ret = hexstring2array(arr, buf);
        break;
    default:
        break;
    }
    if (ret < 0) {
        return ret;
    }
    len = arr.length() + 6;
    arr.prepend(cmd);
    arr.prepend(type);
    arr.prepend(len & 0xFF);
    arr.prepend((len >> 8) && 0xFF);
    arr.prepend((char)0xFA);
    arr.append((char)0xFD);
    qDebug() << "UsbSerial write: " << arr;
    if (cmd == 4) {
        cal_send_count(type, arr);
    }
    return m_serial.write(arr);
}

int UsbSerial::cal_send_count(char type, QByteArray &arr)
{
    int cnt = 0;
    switch (type) {
    case 1:
        //cnt = (arr.length() - 6) / msg.length;
        break;
    case 7:
    case 8:
    case 9:
        cnt = arr.length() - 6;
        break;
    default:
        break;
    }
    m_SendCount[type] += cnt;
    return 0;
}

int UsbSerial::cal_recv_count(char type, QByteArray &arr)
{
    int cnt = 0;
    switch (type) {
    case 1:

        break;
    case 7:
    case 8:
    case 9:
        cnt = arr.length() - 6;
        break;
    default:
        break;
    }
    m_RecvCount[type] += cnt;
    return 0;
}

QVariant UsbSerial::string2hexstring(QVariant data) {
    QVariant buf;
    QByteArray arr;
    qDebug() << "string2hexstring call string2array ...";
    string2array(arr, data);
    array2hexstring(buf, arr);
    return buf;
}

QVariant UsbSerial::hexstring2string(QVariant data) {
    QVariant buf;
    QByteArray arr;
    qDebug() << "hexstring2string call string2array ...";
    hexstring2array(arr, data);
    array2string(buf, arr);
    return buf;
}
