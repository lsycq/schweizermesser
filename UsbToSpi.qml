﻿import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1
import Qt.UsbSerial 1.0

Rectangle {
    id: root;
    property int spiType: 2;
    Column {
        x: 2;
        y: 4;
        spacing: 4;
        GroupBox {
            id: viewGroupBox;
            width: 650;
            height: 100;
            Column {
                x: 4;
                y: 4;
                spacing: 6;
                Row {
                    spacing: 4;
                    Text {
                        y: 4;
                        text: qsTr("Model：")
                    }
                    ComboBox {
                        id: modelCombox;
                        width: 40;
                        model: [qsTr("主"), qsTr("从")];
                    }
                    Text {
                        y: 4;
                        text: qsTr("Width:")
                    }
                    ComboBox {
                        id: widthCombox;
                        width: 48;
                        model: ["8", "16"];
                    }
                    Text {
                        y: 4;
                        text: qsTr(" CPOL：")
                    }
                    ComboBox {
                        id: cpolCombox;
                        width: 40;
                        model: ["0", "1"];
                    }
                    Text {
                        y: 4;
                        text: qsTr(" CPHA：")
                    }
                    ComboBox {
                        id: cphaCombox;
                        width: 40;
                        model: ["0", "1"];
                    }
                    Text {
                        y: 4;
                        text: qsTr(" CS：")
                    }
                    ComboBox {
                        id: csCombox;
                        width: 75;
                        model: ["低有效", "高有效"];
                    }
                    Text {
                        y: 4;
                        text: qsTr(" Send：");
                    }
                    ComboBox {
                        id: sendPCombox;
                        width: 40;
                        model: ["低", "高"];
                    }
                    ComboBox {
                        id: divisionCombox;
                        width: 54;
                        model: ["2", "4", "8", "16", "32", "64", "128", "256"];
                    }
                }
                Row {
                    spacing: 4;
                    Text {
                        y: 4;
                        text: qsTr("Send Data:");
                    }
                    TextField {
                        id: dataText;
                        width: 400;
                    }
                    Button {
                        text: "Send";
                        width:50;
                        onClicked: {
                            var data;
                            data = functionItem.formatHexString(dataText.text);
                            data = functionItem.cutHexStr(data);
                            functionItem.sendData(spiType, 4, data, UsbSerial.HexStringType);
                        }
                    }
                    Button {
                        text: "Recv";
                        width:50;
                        onClicked: {
                            functionItem.sendData(spiType, 2, "", UsbSerial.HexStringType);
                        }
                    }

                    Button {
                        text: "Config";
                        width: 50;
                        onClicked: {
                            var dataString;
                            var model = (Number(modelCombox.currentIndex)).toString();
                            var width = (Number(widthCombox.currentIndex)).toString();
                            var cpol = (Number(cpolCombox.currentIndex)).toString();
                            var cpha = (Number(cphaCombox.currentIndex)).toString();
                            var cs = (Number(csCombox.currentIndex)).toString();
                            var division = (Number(divisionCombox.currentIndex)).toString();
                            //var baud = functionItem.numberToBytes(baudText.text, true);
                            var send = (Number(sendPCombox.currentIndex)).toString();
                            var config = model + " " + width + " " + cpol + " " + cpha + " " + cs + " " + division + " " + send;
                            console.log("config = " +  config);
                            functionItem.sendData(2, 3, config, UsbSerial.HexStringType);
                        }
                    }
                }
                Row {
                    spacing: 4;
                    Text {
                        y: 4;
                        text: qsTr("Recv Data:");
                    }
                    TextField {
                        id: recvDataText;
                        width: 400;
                    }
                }
            }
        }
    }
    //更新页面参数
    function updataSpiConfig(type, cmd, data) {
        if (type == spiType && cmd == 3) {
            var s = data.split(" ");
            //判断返回的数据是否小于combo.count；
            var model = parseInt(s[0], 16);
            var width = parseInt(s[1], 16);
            var cpol = parseInt(s[2], 16);
            var cpha = parseInt(s[3], 16);
            var cs = parseInt(s[4], 16);
            var division = parseInt(s[5], 16);
            var sendP = parseInt(s[6], 16);
            if (model < modelCombox.count) {
                modelCombox.currentIndex = model;
            }
            if (width < widthCombox.count) {
                widthCombox.currentIndex = width;
            }
            if (cpol < cpolCombox.count) {
                cpolCombox.currentIndex = cpol;
            }
            if (cpha < cphaCombox.count) {
                cphaCombox.currentIndex = cpha;
            }
            if (cs < csCombox.count) {
                csCombox.currentIndex = cs;
            }
            if (division < divisionCombox.count) {
                divisionCombox.currentIndex = division;
            }
            if (sendP < sendPCombox.count) {
                sendPCombox.currentIndex = sendP;
            }
        } if (type == spiType && cmd == 5) {
            data = functionItem.dataStr16Str(data);
            recvDataText.text = data;
        } else {
            return -1;
        }
    }
}
