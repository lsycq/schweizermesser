﻿import QtQuick 2.7
import QtCharts 2.1

ChartView {
    id: chartView;
    property alias axisX: axisX;
    animationOptions: ChartView.NoAnimation;
    theme: ChartView.ChartThemeDark;
    Component.onCompleted: {
        dataSource.dataDepth = axisX.max + 1;
//        dataSource.saveData("15 00 12 00 15 00 12 00 15 00 12 00 15 00 12 00 15 00 12 00 15 00 12 00 15 00 12 00 ");
//        dataSource.saveData("15 00 12 00 15 00 12 00 15 00 12 00 15 00 12 00 15 00 12 00 15 00 12 00 15 00 12 00 ");
    }

    ValueAxis {
        id: axisY1;
        min: 0;
        max: 4;
        labelFormat: "%.2f V";
        tickCount: 11;
    }
    // for signal 2
//    ValueAxis {
//        id: axisY2;
//        min: -10;
//        max: 5;
//    }
    ValueAxis {
        id: axisX;
        min: 0;
        max: 128;
        labelFormat: "%.1f ms";
    }
    // signal 1
    LineSeries {
        id: lineSeries1;
        name: "signal 1";
        axisX: axisX;
        axisY: axisY1;
        useOpenGL: true;
    }
    Timer {
        id: refreshTimer;
        interval: 100;      // 1000 ms for test
        running: true;
        repeat: true;
        onTriggered: {
            dataSource.update(chartView.series(0));
            parent.voltage = dataSource.voltage.toString();
        }
    }
}
