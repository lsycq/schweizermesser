import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1
import Qt.UsbSerial 1.0

Rectangle {
    id: root;
    property int iicType: 3;
    Column {
        x: 2;
        y: 4;
        spacing: 4;
        GroupBox {
            id: viewGroupBox;
            ExclusiveGroup {
                id: masg1TypeGroup;
            }
            ExclusiveGroup {
                id: masg2TypeGroup;
            }
            width: 650;
            height: 80;
            Column {
                y: 4;
                spacing: 6;
                Row {
                    x: 4;
                    spacing: 4;
                    Text {
                        y: 4;
                        text: qsTr("器件地址:");
                    }
                    TextField {
                        id: addrText;
                        width: 60;
                    }
                    Text {
                        id: masg1Text;
                        y: 4;
                        text: qsTr("  消息 1:");
                    }
                    RadioButton {
                        id: write1Check;
                        y: 3;
                        text: "W";
                        exclusiveGroup: masg1TypeGroup;
                    }
                    RadioButton {
                        id: read1Check;
                        y: 3;
                        text: "R  ";
                        exclusiveGroup: masg1TypeGroup;
                        onCheckedChanged: {
                            if (checked) {
                                masg1DataText.text = "";
                            } else {
                                masg1LengthText.text = "0";
                            }
                        }
                    }
                    TextField {
                        id: masg1DataText;
                        width: 150;
                        readOnly: write1Check.checked ? false : true;
                        onTextChanged: {
                            if (write1Check.checked) {
                                var s = masg1DataText.text;
                                if (s == "") {
                                    masg1LengthText.text = 0;
                                } else {
                                s = s.split(" ");
                                masg1LengthText.text = s.length;
                                }
                            }
                        }
                    }
                    TextField {
                        id: masg1LengthText;
                        width: 30;
                        readOnly: write1Check.checked ? true : false;
                    }
                    Button {
                        text: "Send";
                        onClicked: {
                            sendIicData(iicType, 4);
                        }
                    }
                }
                Row {
                    x: masg1Text.x;
                    spacing: 4;
                    CheckBox {
                        id: masg2Check;
                        y: 4;
                        text: qsTr("消息 2:");
                    }
                    RadioButton {
                        id: write2Check;
                        y: 3;
                        text: "W";
                        exclusiveGroup: masg2TypeGroup;
                    }
                    RadioButton {
                        id: read2Check;
                        y: 3;
                        text: "R  ";
                        exclusiveGroup: masg2TypeGroup;
                        onCheckedChanged: {
                            if (checked) {
                                masg2DataText.text = "";
                            } else {
                                masg2LengthText.text = "0";
                            }
                        }
                    }
                    TextField {
                        id: masg2DataText;
                        width: 150;
                        readOnly: write2Check.checked ? false : true;
                        onTextChanged: {
                            if (write2Check.checked) {
                                var s = masg2DataText.text;
                                if (s == "") {
                                    masg2LengthText.text = 0;
                                } else {
                                s = s.split(" ");
                                masg2LengthText.text = s.length;
                                }
                            }
                        }
                    }
                    TextField {
                        id: masg2LengthText;
                        width: 30;
                        readOnly: write2Check.checked ? true : false;
                    }
                }
            }
        }
        TextArea {
            id: recvDataText;
            width: viewGroupBox.width;
            height: root.height - viewGroupBox.height - 30;
            readOnly: true;
        }
    }
    function sendIicData(type, cmd) {
        var data, data1, data2, addr, rw1, rw2, length1, length2;
        addr = functionItem.addrData(addrText.text);
        if (write1Check.checked) {
            rw1 = 0;
            data1 = functionItem.formatHexString(masg1DataText.text);
            data1 = functionItem.cutHexStr(data1);
        } else {
            rw1 = 1;
            data1 = "";
            for (var i = 0; i < Number(masg1LengthText.text); i ++) {
                data1 += " " + "0";
            }
        }
        length1 = functionItem.addrData(Number(masg1LengthText.text));
        console.log("sendIicData " + data1);
        data1 = addr + " " + rw1 + " " + length1 + data1;
        data = data1;
        //存在masg2时的数据
        if (masg2Check.checked) {
            if (write2Check.checked) {
                rw2 = 1;
                data2 = functionItem.formatHexString(masg2DataText.text);
                data2 = functionItem.cutHexStr(data2);
            } else {
                rw2 = 1;
                data2 = "";
                for (i = 0; i < Number(masg2LengthText.text); i ++) {
                   data2 +=  " " + "0";
                }
            }
            length2 = functionItem.addrData(Number(masg2LengthText.text));
            data2 = data1 + " " + addr + " " + rw2 + " " + length2 + data2;
            data = data2;
        }
        functionItem.sendData(type, cmd, data, UsbSerial.HexStringType);
    }
    function updataIicConfig(type, cmd, data) {
        if (type == iicType && cmd == 4) {
            var s = functionItem.splitSpace(data);
            var recvString = "";
            //计算出收到的第一帧的数据长度
            var sl = [s[4], s[3]];
            var length = functionItem.bytes2ToNumber(sl);
            //第一帧收到的收据长度和总长度进行比较 小于总长的话返回的数据写在第一个mag后面的数据框中 大于的话写在下面的数据接收框中
            if (s.length > length + 5) {
                if(s[2] == 1) {
                    for (var i = 0; i < length; i++) {
                        recvString += s[5 + i] + " ";
                    }
                    recvString = functionItem.dataStr16Str(recvString);
                    masg1DataText.text = recvString;
                } else if (s[2 + length + 5] == 1) {
                    for (i = 2 + length + 5 + 3; i < s.length; i++) {
                        recvString += s[i] + " ";
                    }
                    recvString = functionItem.dataStr16Str(recvString);
                    masg2DataText.text = recvString;
                }
            } else {
                if(s[2] == 1) {
                    for (i = 0; i < length; i++) {
                        recvString += s[5 + i] + " ";
                    }
                    recvString = functionItem.dataStr16Str(recvString);
                    masg1DataText.text = recvString;
                }
            }
        }
    }
}
