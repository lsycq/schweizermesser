﻿import QtQuick 2.0
import Qt.UsbSerial 1.0

Item {
    id: funcItem;

    //转换为16进制字符串
    function dataStringToData(data) {
        //var dataArr = [];
        var dataString;
        var s = "";
        dataString = data.split(" ");
        var i;
        console.log("dataStringToData = " + dataString.length + " " + dataString);
        for(i = 0; i < dataString.length; i++) {
            //dataArr[i] = dataString[i];
            dataString[i] = parseInt(dataString[i]);
            s += " " + dataString[i].toString(16);
        }
        console.log("dataStringToData = " + s);
        return s;
    }
    // input string("1 2 3 4"), return arry "dataArr[4] = {1, 2, 3, 4}"
    function dataStringToArr(data) {
        var dataArr = [];
        var dataString;
        dataString = data.split(" ");
        var i;
        for(i = 0; i < dataString.length; i++) {
            dataArr[i] = dataString[i].toString(16);
        }
        return dataArr;
    }
    // input number(0x12345), return hex string "45 23 1 0"
    function idData(dataString) {
        var buf = new ArrayBuffer(4);
        var dv = new DataView(buf);
        console.log("idData " + parseInt(dataString, 16));
        dv.setUint32(0, parseInt(dataString, 16), true);
        var n = [];
        n[0] = dv.getUint8(0);
        n[1] = dv.getUint8(1);
        n[2] = dv.getUint8(2);
        n[3] = dv.getUint8(3);
        dataString = n[0].toString(16) + " " + n[1].toString(16) + " " + n[2].toString(16) + " " + n[3].toString(16);
        console.log("idData = " + dataString);
        return dataString;
    }
    // input number(0x12345), return hex string "45 23"
    function addrData(dataString) {
        var buf = new ArrayBuffer(2);
        var dv = new DataView(buf);
        dv.setUint16(0, parseInt(dataString, 16), true);
        var n = [];
        n[0] = dv.getUint8(0);
        n[1] = dv.getUint8(1);
        dataString = n[0].toString(16) + " " + n[1].toString(16);
        console.log("addrData = " + dataString);
        return dataString;
    }

    //将接收到的字节数写入
    function recvCountText(){
        var count;
        if (frame.currentIndex == 0) {
            if(frame.getTab(0).item.uartType == 7) {
                count = usbSerial.recvCountRS232;
                console.log("recvCountText " + count )
            } else if(frame.getTab(0).item.uartType == 8) {
                count = usbSerial.recvCountRS485;
            } else if(frame.getTab(0).item.uartType == 9) {
                count = usbSerial.recvCountTTL;
            }
            frame.getTab(0).item.recvText.text = count;
        } else if(frame.currentIndex == 1) {
//            count = usbSerial.recvCountCAN;
//            frame.getTab(1).item.recvTextField.text = count;
        }
    }
    //将发送的字节数写入
    function sendCountText(type){
        if (frame.currentIndex == 0) {
            var count;
            if(type == 7) {
                count = usbSerial.sendCountRS232;
            } else if(type == 8) {
                count = usbSerial.sendCountRS485;
            } else if(type == 9) {
                count = usbSerial.sendCountTTL;
            } else {
                return;
            }
            frame.getTab(0).item.sendText.text = count;
        } else if (frame.currentIndex == 1) {
//            if (type == 1) {
//                count = usbSerial.sendCountCAN;
//            } else {
//                return;
//            }
//            frame.getTab(1).item.sendTextField.text = count;
        }
    }
    function sendData(type, cmd, data, stringType) {
        var dataStrHex;
        console.log("send Data write: " + data + " type " + stringType);
        var ret = usbSerial.write(type, cmd, data, stringType);
        if (ret < 0) {
            return;
        }

        if (stringType == UsbSerial.HexStringType) {
            dataStrHex = data;
            console.log("sendData data1 = " + data);
            if (frame.currentIndex == 0 && type == frame.getTab(0).item.uartType) {
                data = usbSerial.hexstring2string(data);
                console.log("sendData data2 = " + data);
            }
        } else if (stringType == UsbSerial.StringType) {
            dataStrHex = usbSerial.string2hexstring(data);
        }
        sendCountText(type);
        dataAreaView(type, cmd, dataStrHex, data, true);
        textData.log("Send Data:\n" + _number16Format(type) + " " + _number16Format(cmd) + " " + dataStrHex);
    }

    function recvData(type, cmd, data) {
        console.log("data = " + data);
        if (type == 0 && cmd == 1 && data == 1) {
            //connect success
            //textData.connectButton.color = "lightgreen";
        } else if (type == 0 && cmd == 2) {
            var arr = dataStringToArr(data);
            var v = "V" + arr[0] + "." + arr[1];
            //get version
            textData.getVTextField.text = v;
        }
        if (frame.currentIndex == 0) {
            frame.getTab(0).item.updataParamConfig(type, cmd, data);
        }
        if (frame.currentIndex == 1) {
                frame.getTab(1).item.updataCanConfig(type, cmd, data);
        }
        if (frame.currentIndex == 2) {
            frame.getTab(2).item.updataSpiConfig(type, cmd, data);
        }
        if (frame.currentIndex == 3) {
            frame.getTab(3).item.updataIicConfig(type, cmd, data);
        }
        if (frame.currentIndex == 4) {
            frame.getTab(4).item.updataAdcConfig(type, cmd, data);
        }
        //收到数据转换为16进制字符串
        var dataStrHex = dataStr16Str(data);
        var dataString = "Recv Data:\n" + _number16Format(type) + " " + _number16Format(cmd) + " " + data;
        dataAreaView(type, cmd, dataStrHex, data, false);
        recvCountText();
        textData.log(dataString);
    }

    //uart显示区显示数据 判断是否为16进制显示 如果send为true 显示为发送区
    function dataAreaView(type, cmd, dataStrHex, dataStr, send) {
        var dataString;
        if (send) {
            if (frame.getTab(0).item.hexViewCheckBox.checked) {
                dataString = dataStrHex;
                dataString = dataStr16Str(dataString);
                console.log("dataView = " + dataStrHex + " " + dataString)
            } else {
                dataString = dataStr;
            }
        } else {
            if (frame.getTab(0).item.hexViewCheckBox.checked) {
                dataString = dataStrHex;
                dataString = dataStr16Str(dataString);
            } else {
                dataString = dataStr;
            }
        }
        console.log("send2 = " + send + " " + dataString);
        if ((type == 7 || type == 8 || type == 9) && (cmd == 4 || cmd == 5)) {
            if (send) {
                dataString = dataString.trim();
                frame.getTab(0).item.sendDataArea(dataString);
            } else {
                if (type == frame.getTab(0).item.uartType) {
                    dataString = dataString.trim();
                    frame.getTab(0).item.recvDataArea(dataString);
                }
            }
        }
    }
    // input string("1 2 3 4 5"), return hex string "01 02 03 04 05"
    function dataStr16Str(data) {
        var dataString;
        var s = "";
        dataString = data.split(" ");
        s = number16Format(dataString);
        return s;
    }

    function _number16Format(n) {
        var s = n.toString(16);
        if (s.length == 1) {
            s = "0" + s;
        }
        //s = "0x" + s;
        return s;
    }

    function number16Format(n) {
        var i;
        var s = "";
        for (i = 0; i < n.length; i++) {
            s += _number16Format(n[i]) + " ";
        }
        return s;
    }
    //去掉多余的字符
    // "1234 5678" => "34 78"
    function cutHexStr(data) {
        var s = data.split(" ");
        var i;
        var r = "";
        if (data != "") {
            for (i = 0; i < s.length; i++) {
                var x = parseInt(s[i], 16);
                x &= 0xFF;
                r +=  " " + x.toString(16);
            }
        }
        return r;
    }

    // input array: x[] = ["1", "2", "3"], return number(0x030201)
    function bytesToNumber(x) {
        var buf = new ArrayBuffer(x.length);
        var dv = new DataView(buf);
        var i;
        for (i = 0; i < x.length; i++) {
            dv.setUint8(i, parseInt(x[i], 16), true);
        }
        return dv.getUint32(0);
    }
    // input array: x[] = ["1", "2"], return number(0x0201)
    function bytes2ToNumber(x) {
        var buf = new ArrayBuffer(x.length);
        var dv = new DataView(buf);
        var i;
        for (i = 0; i < x.length; i++) {
            dv.setUint8(i, parseInt(x[i], 16), true);
        }
        return dv.getUint16(0);
    }
    // input number(12345), if reverse is true; return hex string "45 23 1 0"  else return "12 34 5 0"
    function numberToBytes(x, reverse) {
        var buf = new ArrayBuffer(4);
        var dv = new DataView(buf);
        dv.setUint32(0, parseInt(x, 10), true);
        var n = [];
        n[0] = dv.getUint8(0);
        n[1] = dv.getUint8(1);
        n[2] = dv.getUint8(2);
        n[3] = dv.getUint8(3);
        if (reverse) {
            x = n[0].toString(16) + " " + n[1].toString(16) + " " + n[2].toString(16) + " " + n[3].toString(16);
        } else {
            x = n[3].toString(16) + " " + n[2].toString(16) + " " + n[1].toString(16) + " " + n[0].toString(16);
        }
        return x;
    }
    // input number(12345)   return "54 32"
    function numberTo2Bytes(x) {
        var buf = new ArrayBuffer(2);
        var dv = new DataView(buf);
        dv.setUint16(0, parseInt(x, 10), true);
        var n = [];
        n[0] = dv.getUint8(0);
        n[1] = dv.getUint8(1);
        x = n[0].toString(16) + " " + n[1].toString(16);
        return x;
    }

    // format hexstring "12  23   25 33    " => "12 23 25 33"
    function formatHexString(s) {
        //        var v = s.split(/(\s+)/);
        var v = s.split(/(\s+)/).filter( function(e) { return e.trim().length > 0; } );
        console.log(v);
        var i;
        s = "";
        for (i = 0; i < v.length; i++) {
            s += v[i];
            if (i != (v.length - 1)) {
                s += " ";
            }
        }
        return s;
    }
    function splitSpace(s) {
        return s.split(/(\s+)/).filter( function(e) { return e.trim().length > 0; } );
    }
}
