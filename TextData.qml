﻿import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import Qt.UsbSerial 1.0

GroupBox {
    id: textGropBox;
    width: 170;
    height: 426;
    property alias getVTextField: versionText;
    //property alias connectButton: connectButton;
    Column {
        spacing: 2;
        GroupBox {
            id: openGropBox;
            width: textGropBox.width - 14;
            height: 90;
            Column {
                spacing: 6;
                Row {
                    x: 4;
                    y: 4;
                    spacing: 6;
                    ComboBox {
                        id: comComBox;
                        width: 70;
                        height: 20;
                        model: ListModel {
                            id: model;
                        }
                        Component.onCompleted: {
                            var list = usbSerial.scan();
                            var i;
                            for (i = 0; i < list.length; i++) {
                                comComBox.model.append({text: list[i]});
                            }
                        }
                    }
                    Button {
                        id: openButton;
                        text: "OPEN";
                        width: 60;
                        height: 20;
                        property bool isOpen: false
                        onClicked: {
                            if (isOpen) {
                                if (usbSerial.close()) {
                                    openButton.text = "OPEN";
                                    isOpen = false;
                                    connectButton.color = "#EEEEEE";
                                }
                            } else {
                                if (usbSerial.open(comComBox.currentText)) {
                                    openButton.text = "CLOSE";
                                    isOpen = true;
                                    frameChange(frame.currentIndex);
                                    functionItem.sendData(0, 1, "1", UsbSerial.HexStringType);
                                    console.log("open success. && connect success.")
                                }
                            }
                        }
                    }
                }
                Row {
                    x: 4;
                    spacing: 16;
                    Button {
                        id: getVButton;
                        width: 60;
                        height: 20;
                        text: "GET";
                        onClicked: {
                            functionItem.sendData(0, 2, "1", UsbSerial.HexStringType)
                        }
                    }
                    TextField {
                        id: getVTextField;
                         width: 60;
                         height: 20;
                         readOnly: true;
                         Text {
                             id: versionText;
                             anchors.centerIn: parent;
                         }
                    }
                }
                Row {
                    x: 4;
                    spacing: 16;
//                    Rectangle {
//                        id: connectButton;
//                        width: 60;
//                        height: 20;
//                        color: "#EEEEEE";
//                        property bool hovered: false;
//                        border.color: "black";
//                        border.width: hovered ? 2 : 1;
//                        signal clicked;
//                        Text {
//                            text: qsTr("CONNECT");
//                            anchors.centerIn: parent;
//                        }
//                        MouseArea {
//                                id: ma;
//                                anchors.fill: parent;
//                                hoverEnabled: true;
//                                onEntered: {
//                                    connectButton.hovered = true;
//                                }
//                                onExited: {
//                                    connectButton.hovered = false;
//                                }
//                                onClicked: {
//                                    connectButton.hovered = false;
//                                    connectButton.clicked();
//                                }
//                            }
//                    }
                    Button {
                        text: "clear";
                        onClicked: {
                            clear();
                        }
                    }
                    Button {
                        id: serchBtn;
                        width: 60;
                        height: 20;
                        text: "Serch";
                        onClicked: {
                            var list = usbSerial.scan();
                            var i;
                            for (i = 0; i < list.length; i++) {
                                comComBox.model.clear();
                                comComBox.model.append({text: list[i]});
                            }
                        }
                    }
                }
            }
        }
        Rectangle {
            width: openGropBox.width;
            height: textGropBox.height - openGropBox.height - openGropBox.width - 28;
            border.color: "#AAAAAA";
            radius: 2;
            TextArea {
                id: testData;
                anchors.fill: parent;
            }
        }
        Row {
            spacing: 10;
            Image {
                width: openGropBox.width;
                height: openGropBox.width;
                source: "Donate.png";
            }
        }
        Text {
            color: "blue"
            x: 65;
            text: qsTr("打赏...")
        }
    }

    function log(data) {
        testData.append(data);
    }
    function clear() {
        testData.text = "";
    }
}

