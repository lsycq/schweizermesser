﻿#ifndef DATASOURCE_H
#define DATASOURCE_H

#include <QObject>
#include <QtCharts/QAbstractSeries>
QT_CHARTS_USE_NAMESPACE

// 用于处理scope数据
class DataSource : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int dataDepth READ dataDepth WRITE setDataDepth)
    Q_PROPERTY(QString voltage READ voltage)
public:
    explicit DataSource(QObject *parent = 0);
    int dataDepth()
    {
        return m_data_depth;
    }
    void setDataDepth(int d)
    {
        m_data_depth = d;
        m_data_valid = -1;
        m_voltage = 0;
        clearData();
    }
    QString voltage()
    {
        return QString::number(m_voltage, 'g', 4);
    }

    Q_INVOKABLE void saveData(QString s);

private:
    // 返回下一个应该填充的buffer指针
    QVector<QPointF>* getBuffer()
    {
        if (m_data_valid == -1) {
            // 开始填buf 0
            return &m_data[0];
        } else if (m_data_valid == 0) {
            // 当前缓冲0有效
            return &m_data[1];
        } else if (m_data_valid == 1) {
            return &m_data[0];
        }
        return NULL;
    }
    QVector<QPointF>* changeBuffer()
    {
        if (m_data_valid == -1) {
            // 开始填buf 0
            m_data_valid = 0;
            m_data[1].clear();
            return &m_data[1];
        } else if (m_data_valid == 0) {
            m_data_valid = 1;
            // 当前缓冲0有效
            m_data[0].clear();
            return &m_data[0];
        } else if (m_data_valid == 1) {
            // 当前缓冲1有效
            m_data_valid = 0;
            m_data[1].clear();
            return &m_data[1];
        }
        return NULL;
    }

signals:

public slots:
    void clearData();
    void update(QAbstractSeries *series);

private:
    QVector<QPointF> m_data[2]; // 数据缓冲
    int m_data_depth;       // 存储深度
    int m_data_valid;       // 哪个数据有效
    double m_voltage;
};

#endif // DATASOURCE_H
