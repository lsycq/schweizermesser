﻿#include <QtWidgets/QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "usbserial.h"
#include "datasource.h"

int main(int argc, char *argv[])
{
    // Qt Charts uses Qt Graphics View Framework for drawing, therefore QApplication must be used.
    QApplication app(argc, argv);
    qmlRegisterType<UsbSerial>("Qt.UsbSerial", 1, 0, "UsbSerial");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    DataSource dataSource;
    engine.rootContext()->setContextProperty("dataSource", (QObject *)&dataSource);

    return app.exec();
}
