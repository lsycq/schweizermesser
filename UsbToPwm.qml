import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import Qt.UsbSerial 1.0

Rectangle {
    id: root;
    property int pwmType: 5;
    Column {
        spacing: 10;
        y: (root.height - height) / 3;
        x: (root.width - width) / 2;
        GroupBox {
            id: rootGrop;
            title: "PWM"
            width: 550;
            height: 160;
            Text {
                id: freText;
                y: 4;
                anchors.left: parent.left;
                anchors.leftMargin: 50;
                anchors.top: parent.top;
                anchors.topMargin: 20;
                text: qsTr("频率：  ")
            }
            TextField {
                id: freTextField;
                anchors.left: freText.right;
                anchors.top: freText.top;
                anchors.topMargin: -4;
                anchors.leftMargin: 4;
                width: 50;
                text: qsTr("0")
            }
            Text {
                id: hzText;
                y: 4;
                anchors.left: freTextField.right;
                anchors.leftMargin: 4;
                anchors.top: parent.top;
                anchors.topMargin: 20;
                text: qsTr("kHz")
            }
            Text {
                id: dutyText;
                y: 4;
                anchors.left: freText.left;
                anchors.top: freText.bottom;
                anchors.topMargin: 20;
                text: qsTr("占空比：")
            }
            TextField {
                id: dutyTextField;
                anchors.left: freTextField.left;
                anchors.top: dutyText.top;
                anchors.topMargin: -4;
                width: 50;
                text: "0";
                validator: IntValidator{
                    bottom: 0;
                    top: 100;
                }
            }
            Slider {
                id: dutySlider;
                anchors.left: dutyTextField.right;
                anchors.top: dutyTextField.top;
                anchors.leftMargin: 10;
                anchors.topMargin: -4;
                minimumValue: 0;
                maximumValue: 100;
                value: Number(dutyTextField.text);
                onValueChanged: {
                    dutyTextField.text = value;
                }
                stepSize: 1;
                style: SliderStyle {
                    groove: Rectangle {
                        implicitWidth: 240;
                        implicitHeight: 8;
                        color: "#66B3FF";
                        radius: 8;
                    }
                    handle: Rectangle {
                        anchors.centerIn: parent;
                        color: control.pressed ? "white" : "lightgray";
                        border.color: "gray";
                        border.width: 1;
                        width: 26;
                        height: 26;
                        radius: 8;
                        Text {
                            anchors.centerIn: parent;
                            text: control.value.toFixed(0);
                            color: "red";
                        }
                    }
                }
            }
            Button {
                id: sendBtn;
                text: qsTr("开始");
                width: 50;
                anchors.left: freText.left;
                anchors.top: dutyText.bottom;
                anchors.topMargin: 40;
                onClicked: {
                    pwmSendData(true);
                }
            }
            Button {
                id: recvBtn;
                text: qsTr("结束");
                width: 50;
                anchors.left: sendBtn.right;
                anchors.leftMargin: 10;
                anchors.top: sendBtn.top;
                onClicked: {
                    pwmSendData(false);
                }
            }
        }
        GroupBox {
            id: daGrop;
            title: "DA"
            width: rootGrop.width;
            height: 100;
            Column {
                spacing: 20;
                x: 50;
                Row {
                    spacing: 4;
                    Text {
                        y: 4;
                        text: qsTr("电压值：")
                    }
                    TextField {
                        id: voltageText;
                        width: 40;
                        text: "0";
                        validator: DoubleValidator{
                            top: 3.3;
                            bottom: 0;
                        }
                    }
                    Text {
                        y: 4;
                        text: qsTr("V  ");
                    }
                    Slider {
                        id: voltageSlider;
                        minimumValue: 0;
                        maximumValue: 3.3;
                        value: Number(voltageText.text);
                        onValueChanged: {
                            voltageText.text = value.toFixed(1);
                        }
                        stepSize: 0.1;
                        style: SliderStyle {
                            groove: Rectangle {
                                implicitWidth: 240;
                                implicitHeight: 8;
                                color: "#66B3FF";
                                radius: 8;
                            }
                            handle: Rectangle {
                                anchors.centerIn: parent;
                                color: control.pressed ? "white" : "lightgray";
                                border.color: "gray";
                                border.width: 1;
                                width: 26;
                                height: 26;
                                radius: 8;
                                Text {
                                    anchors.centerIn: parent;
                                    text: control.value.toFixed(1);
                                    color: "red";
                                }
                            }
                        }
                    }
                }
                Row {
                    spacing: 4;
                    Button {
                        text: "设置";
                        width: 50;
                        onClicked: {
                            var data;
                            var d = voltageText.text * 1000;
                            if (d > 3300) {
                                d = 3300;
                            }
                            data = functionItem.numberTo2Bytes(d);
                            functionItem.sendData(6, 4, data, UsbSerial.HexStringType);
                        }
                    }
                    Button {
                        text: qsTr("使能");
                        width: 50;
                        onClicked: {
                            functionItem.sendData(6, 3, "1", UsbSerial.HexStringType);
                        }
                    }
                    Button {
                        text: qsTr("禁用");
                        width: 50;
                        onClicked: {
                            functionItem.sendData(6, 3, "0", UsbSerial.HexStringType);
                        }
                    }
                }
            }
        }
    }
    function pwmSendData(en) {
        var data;
        if (en) {
            data = "1 ";
        } else {
            data = "0 ";
        }
        var d = Number(freTextField.text);
        data += functionItem.numberToBytes(d * 1000, true) + " " + parseInt(dutyTextField.text).toString(16);
        functionItem.sendData(pwmType, 3, data, UsbSerial.HexStringType);
    }
//    Flickable {
//        anchors.top: rootGrop.bottom;
//        focus: true
//        width: parent.width
//        Text {
//            id: name
//            text: qsTr("text");
//        }
//    }
}
