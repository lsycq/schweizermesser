﻿#ifndef USBSERIAL_H
#define USBSERIAL_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QDebug>
#include <QList>

// UsbSerial 用于管理usb串口设备，并实现数据收发
class UsbSerial : public QObject
{
    Q_OBJECT
    Q_ENUMS(DataType)
    Q_PROPERTY(int sendCountRS232 READ sendCountRS232 WRITE setSendCountRS232)
    Q_PROPERTY(int recvCountRS232 READ recvCountRS232 WRITE setRecvCountRS232)
    Q_PROPERTY(int sendCountRS485 READ sendCountRS485 WRITE setSendCountRS485)
    Q_PROPERTY(int recvCountRS485 READ recvCountRS485 WRITE setRecvCountRS485)
    Q_PROPERTY(int sendCountTTL READ sendCountTTL WRITE setSendCountTTL)
    Q_PROPERTY(int recvCountTTL READ recvCountTTL WRITE setRecvCountTTL)
    Q_PROPERTY(int sendCountCAN READ sendCountCAN WRITE setSendCountCAN)
    Q_PROPERTY(int recvCountCAN READ recvCountCAN WRITE setRecvCountCAN)
public:
    enum DataType {
        ListType,
        StringType,
        HexStringType
    };
    enum ProtocolType {
        TYPE_INFO,
        TYPE_CAN,
        TYPE_SPI,
        TYPE_IIC,
        TYPE_ADC,
        TYPE_PWM,
        TYPE_DAC,
        TYPE_RS232,
        TYPE_RS485,
        TYPE_UART,
        TYPE_NUM
    };

    int sendCountRS232() {
        return m_SendCount[TYPE_RS232];
    }
    void setSendCountRS232(int n) {
        m_SendCount[TYPE_RS232] = n;
    }
    int recvCountRS232() {
        return m_RecvCount[TYPE_RS232];
    }
    void setRecvCountRS232(int n) {
        m_RecvCount[TYPE_RS232] = n;
    }
    int sendCountRS485() {
        return m_SendCount[TYPE_RS485];
    }
    void setSendCountRS485(int n) {
        m_SendCount[TYPE_RS485] = n;
    }
    int recvCountRS485() {
        return m_RecvCount[TYPE_RS485];
    }
    void setRecvCountRS485(int n) {
        m_RecvCount[TYPE_RS485] = n;
    }
    int sendCountTTL() {
        return m_SendCount[TYPE_UART];
    }
    void setSendCountTTL(int n) {
        m_SendCount[TYPE_UART] = n;
    }
    int recvCountTTL() {
        return m_RecvCount[TYPE_UART];
    }
    void setRecvCountTTL(int n) {
        m_RecvCount[TYPE_UART] = n;
    }
    int sendCountCAN() {
        return m_SendCount[TYPE_CAN];
    }
    void setSendCountCAN(int n) {
        m_SendCount[TYPE_CAN] = n;
    }
    int recvCountCAN() {
        return m_RecvCount[TYPE_CAN];
    }
    void setRecvCountCAN(int n) {
        m_RecvCount[TYPE_CAN] = n;
    }

    Q_INVOKABLE void setOutType(DataType t)
    {
        m_out_type = t;
    }

    explicit UsbSerial(QObject *parent = 0);
    ~UsbSerial();
    Q_INVOKABLE QVariantList scan();
    Q_INVOKABLE bool open(QVariant name);
    Q_INVOKABLE bool close();
    Q_INVOKABLE int write(char type, char cmd, QVariant buf, DataType t);
    QVariantList serial_port_scan();    // 扫描串口设备，找到usb设备，并返回name List
    Q_INVOKABLE QVariant string2hexstring(QVariant data);
    Q_INVOKABLE QVariant hexstring2string(QVariant data);
private:
    bool is_frame_over(QByteArray &arr);
    int valist2array(QByteArray &arr, QVariant &data);
    int string2array(QByteArray &arr, QVariant &data);
    int hexstring2array(QByteArray &arr, QVariant &data);
    int array2valist(QVariant &data, QByteArray &arr);
    int array2string(QVariant &data, QByteArray &arr);
    int array2hexstring(QVariant &data, QByteArray &arr);
    int cal_send_count(char type, QByteArray &arr);
    int cal_recv_count(char type, QByteArray &arr);
signals:
    void readData(char type, char cmd, QVariant buf);
public slots:
    void readyRead();
private:
    QSerialPort m_serial;
    QList<QSerialPortInfo> m_info;
    bool m_isOpened;
    DataType m_out_type;    // readData返回的数据类型
    QByteArray m_buffer;    // 读数据buffer
    int m_SendCount[TYPE_NUM];  //发送返回字节数
    int m_RecvCount[TYPE_NUM];  //接收返回字节数
};

#endif // USBSERIAL_H
