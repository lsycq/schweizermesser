﻿#include "datasource.h"
#include <QDebug>
#include <QtCharts/QXYSeries>
#include <QtCharts/QAreaSeries>
#include <QtCore/QtMath>

Q_DECLARE_METATYPE(QAbstractSeries *)
Q_DECLARE_METATYPE(QAbstractAxis *)

DataSource::DataSource(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QAbstractSeries*>();
    qRegisterMetaType<QAbstractAxis*>();
    m_data_valid = -1;
    m_data_depth = 1024;
    m_voltage = 0.0;
}

void DataSource::update(QAbstractSeries *series)
{
    if (m_data_valid < 0) {
        return;
    }
    if (series) {
        QXYSeries *xySeries = static_cast<QXYSeries *>(series);
        // Use replace instead of clear + append, it's optimized for performance
        xySeries->replace(m_data[m_data_valid]);
    }
}

void DataSource::clearData()
{
    // Remove previous data
    m_data[0].clear();
    m_data[1].clear();
}

void DataSource::saveData(QString s)
{
    QStringList list;
    int i = 0, j = 0;
    qint16 tmp;
    QVector<QPointF> *dv = getBuffer();
    j = dv->count();
    list = s.split(' ');
    foreach (const QString &v, list) {
        if (i & 1) {
            tmp |= v.toUShort(Q_NULLPTR, 16) << 8;
            float x = j;
            float y = (float)tmp / (1 << 12) * 3.3;
            dv->append(QPointF(x, y));
            if (dv->count() == m_data_depth) {
                // 计算电压平均值
                foreach (const QPointF &x, *dv) {
                    m_voltage += x.y();
                }
                m_voltage /= m_data_depth;

                dv = changeBuffer();
                j = 0;
            } else {
                j++;
            }
        } else {
            tmp = v.toUShort(Q_NULLPTR, 16);
        }
        i++;
    }
}
