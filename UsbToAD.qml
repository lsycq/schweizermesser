﻿import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import Qt.UsbSerial 1.0

Rectangle {
    id: adRoot;
    property int adcType: 4;
    Column {
        y: 12;
        spacing: 4;
        Row {
            x: 20;
            spacing: 150;
            Row {
                spacing: 4;
                Text {
                    text: qsTr("间隔:");
                    y: 4;
                }
                TextField {
                    id: interval;
                    width: 60;
                    text: "33";
                }
                Text {
                    text: qsTr("us  ");
                    y: 4;
                }
                Text {
                    text: qsTr("存储深度:");
                    y: 4;
                }
                TextField {
                    id: depth;
                    width: 60;
                    text: "128";
                }
            }
            Row {
                spacing: 8;
                Button {
                    id:startBtn;
                    text: "开始采样"
                    onClicked: {
                        adcConfig(true);
                        scopeView.axisX.max = (Number(interval.text) * Number(depth.text)) / 1000;
                    }
                }
                Button {
                    id:stopBtn;
                    text: "停止"
                    onClicked: {
                        adcConfig(false);
                    }
                }
            }
        }
        ScopeView {
            id: scopeView;
            property alias voltage: voltageText.text;
            width: adRoot.width;
            height: adRoot.height - 40;
            Row {
                x: parent.width - 160;
                y: 20;
                spacing: 4;
                Text {
                    y: 4;
                    text: qsTr("电压：");
                    color: "white";
                }
                TextField {
                    id: voltageText;
                    width: 40;
                    readOnly: true;
                    text: "0";
                }
                Text {
                    y: 4;
                    text: qsTr("V");
                    color: "white";
                }
            }
        }
    }

    function adcConfig(en) {
        var data;
        if (en) {
            data = "1 ";
        } else {
            data = "0 ";
        }
        var d = Number(interval.text);
        if (d < 10) {
            d = 10;
            interval.text = d;
        }
        scopeView.axisX.max = Number(depth.text);
        dataSource.dataDepth = Number(depth.text) + 1;
        data += functionItem.numberToBytes(d * 1000, true) + " " + functionItem.numberTo2Bytes(depth.text);
        functionItem.sendData(adcType, 3, data, UsbSerial.HexStringType);
    }
    function updataAdcConfig(type, cmd, data) {
        if (type == adcType && cmd == 3) {
            var s = data.split(" ");
            var si = [s[4], s[3], s[2], s[1]];
            var i = functionItem.bytesToNumber(si);  //ns
            i = i / 1000;  //us
            interval.text = i;
            var sd = [s[6], s[5]];
            var d = functionItem.bytes2ToNumber(sd);
            depth.text = d;
            scopeView.axisX.max = (Number(interval.text) * Number(depth.text)) / 1000;
        } else {
            return -1;
        }
    }
}

