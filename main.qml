﻿import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import Qt.UsbSerial 1.0

Window {
    id: root;
    visible: true;
    maximumHeight: 480;
    maximumWidth: 840;
    minimumWidth: 840;
    minimumHeight: 480;
    //    width: 840;
    //    height: 480;
    color: "#EEEEEE";
    title: qsTr("Schweizer Messer");
    UsbSerial {
        id: usbSerial;
        onReadData: {
            if (type != 4) {
                console.log("cmd = " + cmd + "  type =" + type + "  buf = " + buf);
            }
            if (type == 4 && cmd == 5) {
                // adc 数据
                dataSource.saveData(buf);
                return;
            }

            functionItem.recvData(type, cmd, buf);
        }
        Component.onCompleted: {
            setOutType(UsbSerial.HexStringType);
        }
    }
    Function {
        id: functionItem;
    }

    Row {
        Rectangle {
            id: rootRect;
            width: 660;
            height: root.height;

            TabView {
                id: frame;
                anchors.fill: parent;
                anchors.margins: 4
                Tab {
                    title: "USBtoUART";
                    UsbToUart {
                        id: usbToUart;
                    }
                }
                Tab {
                    title: "USBtoCAN";
                    UsbToCan {
                        id: usbToCan;
                    }
                }
                Tab {
                    title: "USBtoSPI";
                    UsbToSpi {
                        id: usbToSpi;
                    }
                }
                Tab {
                    title: "USBtoIIC";
                    UsbToI2c {
                        id: usbToI2c;
                    }
                }
                Tab {
                    title: "USBtoADC";
                    UsbToAD {
                        id: usbToAD;
                        width: 600;
                        height: 400;
                    }
                }
                Tab {
                    title: "PWM&DA";
                    UsbToPwm {
                        id: usbToPwm;
                        width: 600;
                        height: 400;
                    }
                }

                style: TabViewStyle {
                    frameOverlap: 1;
                    tab: Rectangle {
                        color: styleData.selected ? "steelblue" :"lightsteelblue";
                        border.color:  "steelblue";
                        implicitWidth: Math.max(text.width + 4, 80);
                        implicitHeight: 20;
                        radius: 2;
                        Text {
                            id: text;
                            anchors.centerIn: parent;
                            text: styleData.title;
                            color: styleData.selected ? "white" : "black";
                        }
                    }
                    frame: Rectangle { color: "steelblue" }
                }
                onCurrentIndexChanged: {
                    frameChange(frame.currentIndex);
                }
            }
        }
        Rectangle {
            width: root.width - rootRect.width;
            height: root.height;
            TextData {
                id: textData;
                width: root.width - rootRect.width - 6;
                height: root.height - 54;
                y: 27;
            }
        }
    }
    function frameChange(current) {
        if (current == 0) {
            functionItem.sendData(frame.getTab(0).item.uartType, 2, "", UsbSerial.HexStringType);
        } else if (current == 1) {
            frame.getTab(1).item.configSend();
        } else if (current == 2) {
            functionItem.sendData(frame.getTab(2).item.spiType, 2, "", UsbSerial.HexStringType);
        } else if (current == 4) {
            functionItem.sendData(frame.getTab(4).item.adcType, 2, "", UsbSerial.HexStringType);
        }
    }
}
