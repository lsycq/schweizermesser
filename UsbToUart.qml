﻿import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import Qt.UsbSerial 1.0

Rectangle {
    id: root;
    property alias hexViewCheckBox: hexViewCheckBox;
    property alias hexSendCheckBox: hexSendCheckBox;
    property alias uartSendDataArea: uartSendDataArea;
    property alias uartRecvDataArea: uartRecvDataArea;
    property alias sendText: sendText;
    property alias recvText: recvText;
    property int uartType: 0;
    Row {
        id: rootRow;
        x: 2;
        y: 4;
        Column {
            id: sendColum;
            spacing: 4;
            GroupBox {
                id: viewGroupBox;
                width: 650;
                height: 100;
                Column {
                    y: 4;
                    spacing: 8;
                    Row {
                        spacing: 4;

                        Text {
                            id: stopText;
                            y: 4;
                            text: qsTr("停止位:");
                        }
                        ComboBox {
                            id: stopCombo;
                            height: 20;
                            width: 50;
                            model: [1, 1.5, 2];
                        }
                        Text {
                            id: parityText;
                            y: 4;
                            text: qsTr(" 奇偶校验:");
                        }
                        ComboBox {
                            id: parityCombo;
                            width: 80;
                            height: 20;
                            model: [qsTr("无"), qsTr("奇校验"), qsTr("偶校验")];
                        }
                        Text {
                            id: baudText;
                            y: 4;
                            text: qsTr(" 波特率:");
                        }
                        ComboBox {
                            id: baudCombo;
                            width: 80;
                            height: 20;
                            model: [1200, 2400, 4800, 9600, 14400, 19200, 38400, 43000, 57600,
                                76800, 115200, 128000, 230400, 256000, 460800, 921600, 1382400];
                        }
                        Text {
                            id: dataText;
                            y: 4;
                            text: qsTr(" 数据位:");
                        }
                        ComboBox {
                            id: dataCombo;
                            height: 20;
                            width: 50;
                            model: [5, 6, 7, 8, 9];
                        }
                    }
                    Row {
                        spacing: 4;
                        Text {
                            id: sendDataText;
                            y: 4;
                            text: qsTr("数据: ");
                        }
                        TextField {
                            id: sendDataTextEdit;
                            width: 530;
                            height: 20;
                        }
                    }
                    Row {
                        spacing: 10;
                        y: 14;
                        x: sendDataTextEdit.x;
                        CheckBox {
                            id: hexViewCheckBox;
                            text: "16进制显示";
                            //checked: true;
                            onCheckedChanged: {
                                if(checked) {
                                    usbSerial.setOutType(UsbSerial.HexStringType);
                                } else {
                                    usbSerial.setOutType(UsbSerial.StringType);
                                }
                            }
                        }
                        CheckBox {
                            id: timedSendCheckBox;
                            text: "定时发送";
                            onCheckedChanged: {
                                uartTimer.running = checked;
                            }
                        }
                        CheckBox {
                            id: hexSendCheckBox;
                            text: "16进制发送";
                        }
                        CheckBox {
                            id: sendRowCheckBox;
                            text: "发送新行";
                        }
                        Text {
                            id: cycleText;
                            y: sendRowCheckBox.y + 2;
                            text: qsTr("周期：      ms");
                            TextField {
                                id: cycleTextEdit;
                                x: 34;
                                y: -4;
                                width: 34;
                                height: 20;
                                text: "1000";

                            }
                        }
                    }
                }

                Button {
                    id: parameterConfigButton;
                    width: 70;
                    height: 25;
                    x: dataCombo.x + dataCombo.width + 10;
                    y: dataText.y;
                    text: "参数配置";
                    onClicked: {
                        var dataString;
                        var baud;
                        baud = functionItem.numberToBytes(baudCombo.currentText, true);
                        var stop = (Number(stopCombo.currentIndex + 1)).toString();
                        var data = (Number(dataCombo.currentIndex + 5)).toString();
                        var parity = (Number(parityCombo.currentIndex + 1)).toString();
                        var config = baud + " " + stop + " " + data + " " + parity;
                        console.log("config = " +  config)
                        functionItem.sendData(uartType, 3, config, UsbSerial.HexStringType);
                    }
                }
                ComboBox {
                    id: uartChooseCombo;
                    width: 65;
                    anchors.left: parameterConfigButton.right;
                    anchors.leftMargin: 4;
                    anchors.top: parameterConfigButton.top;
                    model: ["RS232", "RS485", "TTL"];
                    onCurrentIndexChanged: {
                        if (currentIndex == 0) {
                            uartType = 7;
                        } else if (currentIndex == 1) {
                            uartType = 8;
                        } else {
                            uartType = 9;
                        }
                        functionItem.sendData(uartType, 2, "", UsbSerial.HexStringType);
                    }
                }
                Button {
                    id: sendButton;
                    width: 70;
                    height: 25;
                    x: parameterConfigButton.x;
                    y: parameterConfigButton.y + parameterConfigButton.height + sendDataTextEdit.height + 6;
                    text: "发送";
                    onClicked: {
                        hexSendData(uartType, 4, sendDataTextEdit.text);
                    }
                }
            }
            Rectangle {
                id: dataViewGroupBox;
                width: viewGroupBox.width;
                height: root.height - viewGroupBox.height - 30;
                Column {
                    spacing: 4;
                    TextArea {
                        id: uartSendDataArea;
                        width: dataViewGroupBox.width;
                        height: dataViewGroupBox.height / 2 - 2;
                    }
                    TextArea {
                        id: uartRecvDataArea;
                        width: dataViewGroupBox.width;
                        height: dataViewGroupBox.height / 2 - 2;
                    }
                }
            }
            Row {
                x: 4;
                spacing: 4;
                Text {
                    text: qsTr("Send Bytes:")
                }
                TextEdit {
                    id: sendText;
                    width: 40;
                    text: "0";
                    readOnly: true;
                }
                Text {
                    text: qsTr("Recv Bytes:")
                }
                TextEdit {
                    id: recvText;
                    width: 40;
                    text: "0";
                    readOnly: true;
                }
                Button {
                    id: cleraBtn;
                    width: 60;
                    height: 20;
                    text: "clear";
                    onClicked: {
                        uartSendDataArea.text = "";
                        uartRecvDataArea.text = "";
                        sendText.text = 0;
                        recvText.text = 0;
                        usbSerial.sendCountRS232 = 0;
                        usbSerial.recvCountRS232 = 0;
                    }
                }
            }
        }
    }
    Timer {
        id: uartTimer;
        interval: cycleTextEdit.text;
        running: false;
        repeat: true;
        onTriggered: {
            hexSendData(uartType, 4, sendDataTextEdit.text);
        }
    }

    function textByte() {

    }
    function sendDataArea(data) {
        if (frame.currentIndex == 0) {
            if (uartSendDataArea.text == "") {
                uartSendDataArea.text += data;
            } else {
                uartSendDataArea.text += " " + data;
            }
        }
    }
    function recvDataArea(data) {
        if (frame.currentIndex == 0) {
            if (uartRecvDataArea.text == "") {
                uartRecvDataArea.text += data;
            } else {
                uartRecvDataArea.text += " " + data;
            }
        }
    }
    function hexSendData(type, cmd, data) {
        var stringType;

        if (timedSendCheckBox.checked) {
            uartTimer.running = true;
        } else {
            uartTimer.running = false;
        }

        if (hexSendCheckBox.checked) {
            stringType = UsbSerial.HexStringType;
            data = functionItem.formatHexString(data);
            data = functionItem.cutHexStr(data);
        } else {
            stringType = UsbSerial.StringType;
        }

        if (sendRowCheckBox.checked) {
            data = data + "\n\r";
        }
        functionItem.sendData(type, cmd, data, stringType);
    }

    //查询232, 485, ttl 的参数配置
    function getParamConfig(data) {
        functionItem.sendData(uartType, 2, "", usbSerial.HexStringType);
    }
    //更新页面参数
    function updataParamConfig(type, cmd, data) {
        if (type == uartType && cmd == 2) {
            var s = data.split(" ");
            var sb = [s[3], s[2], s[1], s[0]];
            var baud = functionItem.bytesToNumber(sb);
            var index = baudCombo.find(baud.toString());
            console.log("updataParamConfig = " + s + " " + " "  + baud + " " + index)
            if (index != -1) {
                baudCombo.currentIndex = index;
            }
            //判断返回的数据是否小于combo.count；
            var stop = parseInt(s[4], 16) - 1;
            var datacom = parseInt(s[5], 16) - 5;
            var parity = parseInt(s[6], 16) - 1;
            if (stop < stopCombo.count) {
                stopCombo.currentIndex = stop;
            }
            if (datacom < dataCombo.count) {
                dataCombo.currentIndex = datacom;
            }
            if (parity < parityCombo.count) {
                parityCombo.currentIndex = parity;
            }
        } else {
            return -1;
        }
    }
}
